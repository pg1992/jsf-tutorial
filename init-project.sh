#! /usr/bin/env bash

# Initilize GIT
# Creates all branches
# Creates all projects

GIT_REMOTE='git@gitlab.com:pg1992/jsf-tutorial'

GROUP_ID='com.tutorialspoint.test'
ARCHETYPE_ID=maven-archetype-webapp

TUTORIALS='first-application managed-beans page-navigation basic-tags
facelet-tags convertor-tags validator-tags datatable composite-components ajax
event-handling jdbc-integration spring-integration expression-language
internationalization'

echo 'Release the Kraken!'

# Initialize GIT
## Clear previous GIT database (WARNING!!!)
rm -rf .git

git init
git remote add origin $GIT_REMOTE
git add README.md && git commit -m 'Add project info'
git add init-project.sh && git commit -m 'Add the script that starts everything'

git checkout -b develop

# Create a branch and a webapp archetype for each tutorial
for tutorial in $TUTORIALS
do

    git checkout -b $tutorial

    mvn archetype:generate \
        -DgroupId='org.moreira.pedro' \
        -DartifactId="$tutorial" \
        -DarchetypeArtifactId='maven-archetype-webapp' \
        -Dversion=0.1 \
        -DinteractiveMode=false

    git add $tutorial
    git commit -m "Add project archetype for $tutorial"
    git push -u origin $tutorial

    git checkout develop

done

git push --all

echo 'Have a nice day.'
