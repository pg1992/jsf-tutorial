# JavaServer Faces Tutorial

## Description

This project follows the tutorial presented on [tutorialspoint][1].

## Usage

### Build

Change to the project directory (`helloworld`) and execute `mvn install`.

### Deploy

Make sure the Tomcat8 server is running by executing
```shell
systemctl status tomcat8.service
```
and copy the file `helloworld/target/helloword.war` to
`/var/lib/tomcat8/webapps/`.

## Repository Details

### Branching Model

This project follows the [famous branching model][2] introduced by Vincent
Driessen.

---

[1]: https://www.tutorialspoint.com/jsf/index.htm
[2]: http://nvie.com/posts/a-successful-git-branching-model/
